How to use this repo
=====================
1. Clone the repo
2. Do npm istall
3. Run the app with ionic serve --lab or ionic serve

This is an Ionic app which is written using AngularJs

Logic
=======
The essence of the game is to react to a specific symbol as quickly as possible once it appears on the screen. The game has several levels, 
each level addning complexity in terms of where the symbol appears, it's color changes etc. The symbol to react to is 'Blue point'. Along with that comes
other symbols such as Plus, Minus etc. 


Level 1:
---------
The 'Blue point' will appear at the middle of the screen. 

Level 2:
---------
The 'Blue point' will appear somewhere else on the screen.

Level 3:
---------
The 'Blue point' will appear at the center of the screen but in different colors.

Level 4:
---------
The 'Blue point' will appear somewhere on the screen but in different colors.

Level 5:
---------
The 'Blue point' and othert symbols will appear somewhere on the screen.

Level 6:
---------
The 'Blue point' and othert symbols will appear in different colors somewhere on the screen.