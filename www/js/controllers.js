angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope, $timeout) {

  $scope.ionicon_class_list=['ion-bag', 'ion-headphone', 'ion-icecream', 'ion-heart', 'ion-chevron-right', 'ion-play', 'ion-minus-round', 'ion-record', 'ion-stop', 'ion-plus']
  $scope.ionicon_class_fake_list1=['ion-heart','ion-icecream','ion-bag','ion-minus-round','ion-headphone','ion-chevron-right','ion-play','ion-stop','ion-plus', 'ion-headphone']
  $scope.ionicon_class_fake_list2=['ion-plus','ion-icecream', 'ion-bag','ion-headphone','ion-heart','ion-chevron-right','ion-play','ion-minus-round','ion-stop', 'ion-headphone']
  
  $scope.ionicon_class_list1=['ion-xbox','ion-ribbon-a', 'ion-trophy', 'ion-ios-football','ion-android-bicycle']
  $scope.ionicon_class_list2=['ion-wrench','ion-scissors', 'ion-gear-b', 'ion-clock','ion-erlenmeyer-flask']
  $scope.ionicon_class_list3=['ion-edit','ion-clipboard', 'ion-calculator', 'ion-printer','ion-calendar']

  $scope.current_symbol_for_list=''
  $scope.current_symbol_for_list1=''
  $scope.current_symbol_for_list2=''
  $scope.current_symbol_for_list3=''
  $scope.level=1
  $scope.disableStart=false
  $scope.disableStop=true
  $scope.levelPrompt=''
  $scope.failurePrompt=''
  $scope.flagLeft=false
  $scope.flagMiddle=false
  $scope.flagRight=false
  $scope.whichSymbol='ion-record'
  var timer

  var i = 0, length = $scope.ionicon_class_list.length;

  $scope.start = function(){


    $scope.levelPrompt='Level: ' + $scope.level
    $scope.failurePrompt=''
    $scope.disableStart=true
    $scope.disableStop=false
    $scope.iterator()
  }

  $scope.iterator = function(){

    if($scope.level===1){

      $scope.flagRight=false
      $scope.flagLeft=false
      $scope.flagMiddle=true
      $scope.current_symbol_for_list2=$scope.ionicon_class_list[i]
      $scope.current_symbol_for_list1=$scope.ionicon_class_fake_list1[i]
      $scope.current_symbol_for_list3=$scope.ionicon_class_fake_list2[i]


      if(++i<length) {

        timer = $timeout($scope.iterator, 700)
      }

      else if(i>=length){
        $scope.current_symbol_for_list2=[]
        $scope.current_symbol_for_list1=[]
        $scope.current_symbol_for_list3=[]
        i=0
        $scope.disableStart=false
        $scope.disableStop=true
        $scope.failurePrompt='Time out! Try again.'
      }
    }

    else if($scope.level===2){

      $scope.flagRight=false
      $scope.flagLeft=false
      $scope.flagMiddle=true

      $scope.current_symbol_for_list1=$scope.ionicon_class_list[i]
      $scope.current_symbol_for_list2=$scope.ionicon_class_fake_list1[i]
      $scope.current_symbol_for_list3=$scope.ionicon_class_fake_list2[i]

      if(++i<length) {
        timer = $timeout($scope.iterator, 700)
      } 
      else if(i>=length){
        $scope.current_symbol_for_list1=[]
        i=0
        $scope.disableStart=false
        $scope.disableStop=true
        $scope.failurePrompt='Time out! Try again.'
      }

    }
  }

  $scope.stopTimer = function(){

    if($scope.current_symbol_for_list2===$scope.whichSymbol){
      console.log("Stop timer middle")
      $timeout.cancel(timer)
      $scope.level++
      $scope.current_symbol_for_list1=[]
      $scope.current_symbol_for_list2=[]
      $scope.current_symbol_for_list3=[]
      i=0
      $scope.disableStart=false
      $scope.disableStop=true
      $scope.levelPrompt='Level 1 finished! Click start to play next level.'
    }

    else if($scope.current_symbol_for_list1===$scope.whichSymbol){
      console.log("Stop timer left")
      $timeout.cancel(timer)
      $scope.level++
      $scope.current_symbol_for_list1=[]
      $scope.current_symbol_for_list2=[]
      $scope.current_symbol_for_list3=[]
      i=0
      $scope.disableStart=false
      $scope.disableStop=true
      $scope.levelPrompt='Level 2 finished! Click start to play next level.'
    }

    else if($scope.current_symbol_for_list3===$scope.whichSymbol){
      console.log("Stop timer right")
      $timeout.cancel(timer)
      $scope.level++
      $scope.current_symbol_for_list1=[]
      $scope.current_symbol_for_list2=[]
      $scope.current_symbol_for_list3=[]
      i=0
      $scope.disableStart=false
      $scope.disableStop=true
      $scope.levelPrompt='Level 2 finished! Click start to play next level.'
    }

    else{
      console.log("else")
      $timeout.cancel(timer)
      $scope.current_symbol_for_list1=[]
      $scope.current_symbol_for_list2=[]
      $scope.current_symbol_for_list3=[]
      i=0
      $scope.disableStart=false
      $scope.disableStop=true
      $scope.failurePrompt='Wrong! Try again.'

    }

  }
})

.controller('ChatsCtrl', function($scope, Chats) {
// With the new view caching in Ionic, Controllers are only called
// when they are recreated or on app start, instead of every page change.
// To listen for when this page is active (for example, to refresh data),
// listen for the $ionicView.enter event:
//
//$scope.$on('$ionicView.enter', function(e) {
//});

$scope.chats = Chats.all();
$scope.remove = function(chat) {
  Chats.remove(chat);
};
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
